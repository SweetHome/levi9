package starter.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonPropertyOrder({
        "provider",
        "title",
        "url",
        "brand",
        "price",
        "unit",
        "isPromo",
        "promoDetails",
        "image"
})
public class ItemInfo {
    @JsonProperty("provider")
    private String provider;
    @JsonProperty("title")
    private String title;
    @JsonProperty("url")
    private String url;
    @JsonProperty("brand")
    private String brand;
    @JsonProperty("price")
    private Double price;
    @JsonProperty("unit")
    private String unit;
    @JsonProperty("isPromo")
    private Boolean isPromo;
    @JsonProperty("promoDetails")
    private String promoDetails;
    @JsonProperty("image")
    private String image;
}
