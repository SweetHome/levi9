package starter.utils;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

@UtilityClass
public class TestHelperUtil {

    public static final String HOST_NAME= "https://waarkoop.com";
    public static final String BASE_URL = "https://waarkoop-server.herokuapp.com/api/v1/search/demo/";

    @NotNull
    public String getRequestedItemFromUrl(String url) {
        return url.replaceAll(BASE_URL, "");
    }
}
