package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import org.assertj.core.api.Assertions;
import starter.dto.ApiError;
//import starter.dto.Detail;
import starter.dto.Detail;
import starter.dto.ItemInfo;
import starter.utils.TestHelperUtil;

import java.util.Arrays;
import java.util.Locale;

public class SearchStepDefinitions {

    private String url = null;
    private Response response = null;

    @When("he calls endpoint {string}")
    public void heCallsEndpoint(String requestUrl) {
        this.url = requestUrl;
        this.response = SerenityRest.given().get(requestUrl);
    }

    @Then("he sees the results displayed for {string}")
    public void heSeesTheResultsDisplayedForApple(String itemName) {
        Assertions.assertThat(response.statusCode()).isEqualTo(200);
        Assertions.assertThat(response.body().as(ItemInfo[].class).length).isNotZero();
        Assertions.assertThat(Arrays.stream(response.body().as(ItemInfo[].class)).findAny().get())
                .hasFieldOrProperty("provider")
                .hasFieldOrProperty("title")
                .hasFieldOrProperty("url")
                .hasFieldOrProperty("brand")
                .hasFieldOrProperty("price")
                .hasFieldOrProperty("unit")
                .hasFieldOrProperty("isPromo")
                .hasFieldOrProperty("promoDetails")
                .hasFieldOrProperty("image");

        Assertions.assertThat(Arrays.stream(response.body().as(ItemInfo[].class))
                        .findAny().get().getTitle().toLowerCase(Locale.ROOT))
                .containsPattern(itemName);
    }

    @Then("he does not see the results")
    public void heDoesNotSeeTheResults() {
        Detail expectedDetails = Detail.builder()
                .error(true)
                .message("Not found")
                .requestedItem(TestHelperUtil.getRequestedItemFromUrl(this.url))
                .servedBy(TestHelperUtil.HOST_NAME)
                .build();

        Assertions.assertThat(response.statusCode()).isEqualTo(404);
        Assertions.assertThat((response.body().as(ApiError.class).getDetail()))
                .hasToString(expectedDetails.toString());
        Assertions.assertThat((response.body().as(ApiError.class).getDetail()))
                .isEqualToComparingFieldByField(expectedDetails);
    }


}
