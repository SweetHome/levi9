Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/test/{product} for getting the products.
### Available products: "apple", "mango", "tofu", "water"
### Prepare Positive and negative scenarios

  Scenario:
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/cola"
    Then he sees the results displayed for "cola"
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/apple"
    Then he sees the results displayed for "apple"
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/tofu"
    Then he does not see the results
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/water"
    Then he does not see the results
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/car"
    Then he does not see the results
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/phone"
    Then he does not see the results
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/pen"
    Then he does not see the results
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/keyboard"
    Then he does not see the results