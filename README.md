# Test Task

### Project location:
``` https://gitlab.com/SweetHome/task.git ```

### To run tests:
```mvn clean install```

### To see report in browser:
```mvn serenity:aggregate```

### To see report in console:
```mvn serenity:aggregate -Dserenity.outputType=console```

### Project structure:
```
src
└── main
    ├── java
        ├── starter
            ├── README.md # application code should be placed here
├── test
         ├── java
            ├── starter
                ├── stepdefinitions
                    └── SearchStepDefinitions.java # step definitions
                ├── dto
                    └── ApiError.java # dto for api error
                    └── Detail.java # dto for detail
                    └── ItemInfo.java # dto for item info
                └── utils
                    └── TestHelperUtil.java # util class for test
                └── TestRunner.java # test runner
         ├── resources
            ├── features
                └── search
                    └── search.feature # feature file
            └── serenity.conf # serenity configuration file
```
